package com.swamptp.walkadog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalkadogApplication {

	public static void main(String[] args) {
		SpringApplication.run(WalkadogApplication.class, args);
	}

}
